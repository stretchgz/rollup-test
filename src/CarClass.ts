export default class Car {
  private name: string;
  private speed: number;
  constructor(n: string = 'Lambo') {
    this.name = n;
    this.speed = 0;
  }

  setSpeed(speed: number): void {
    this.speed = speed;
  }
  getSpeed(): number {
    return this.speed;
  }
  getName(): string {
    return this.name;
  }
}