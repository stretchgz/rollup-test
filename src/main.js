// import moment from 'moment';
import {getJSON, updateJSON, postJSON} from './my-module';
import {age} from './my-module2';
import Person from './PersonClass';

updateJSON('yolo');

console.log(`I am ${age} old.`);

let p = new Person('Derek');
console.log(`Hi, I am ${p.getName()}`);

p.setName('John');
console.log(`Hi, I am ${p.getName()}`);

/////////////////////////////////
let [aa, ...bb] = [1, 2, 3, 4, 5, 6, 7];
let cc = [aa, ...bb];

console.log(aa);
console.log(bb);

/////////////////////////////////
// array.find
const inventory = [
  { name: 'a', q: 1 },
  { name: 'b', q: 12 },
  { name: 'c', q: 123 },
  { name: 'd', q: 1234 },
];

let result = inventory.find(o => {
  return o.name === 'a'
});

console.log(result);

/////////////////////////////////
// array.some
const numba = [1, 2, 3, 4, 5, 654, 43, 5, 3545, 7, 352, 424, 342, 423, 8888];

let r2 = numba.some(e => e < 10);
let r3 = numba.some(e => e > 9e3);

console.log(r2);
console.log(r3);
