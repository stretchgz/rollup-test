export default class Person {
  constructor(name = 'John Dude') {
    this.name = name;
  }
  getName() {
    return this.name;
  }
  setName(n) {
    this.name = n;
  }
}