import {getJSON, postJSON, updateJSON} from './my-module';
import {age} from './my-module2';
import Car from './CarClass';
import Person from './PersonClass';

//////////////////////////////////
let p = new Person('Derek');
console.log(`Hi, I am ${p.getName()}`);
// change name
p.setName('John');
console.log(`    I changed name to: ${p.getName()}`);

let p2 = new Person();
console.log(`What's Person 2 name: ${p2.getName()}`);

//////////////////////////////////
let car = new Car('GTR');
car.setSpeed(60);
car.getSpeed();
console.log(`Car1 name is: ${car.getName()}
    Driving speed: ${car.getSpeed()}`);

let car2 = new Car();
console.log(`Car2 name is: ${car2.getName()}
    Driving speed: ${car2.getSpeed()}`);

//////////////////////////////////

updateJSON('gogo');
console.log(`I am ${age} old!`);

//////////////////////////////////

function f(x, ...y): number {
  // y is an Array
  return x * y.length;
}
let ya = f(3, "hello", true) == 6;
console.log(ya);
