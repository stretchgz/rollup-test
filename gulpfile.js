'use strict';

const del = require('del');
const gulp = require('gulp');
var runSequence = require('run-sequence');
const rollup = require('rollup').rollup;
const commonjs = require('rollup-plugin-commonjs');
const nodeResolve = require('rollup-plugin-node-resolve');
const babel = require('rollup-plugin-babel');
const uglify = require('rollup-plugin-uglify');
const fileSize = require('rollup-plugin-filesize');
const typescript = require('rollup-plugin-typescript');

gulp.task('default', ['clean', 'roll']);
gulp.task('build', (cb) => {
  return runSequence('clean',
    ['roll', 'roll:min', 'tsc'],
    cb);
});

gulp.task('clean', () => del(['dist/']));

gulp.task('roll', () => {
  return rollup({
    entry: 'src/main.js',
    plugins: [
      babel({
        exclude: 'node_modules/**'
      }),
      nodeResolve({
        jsnext: true,
        browser: true
      }),
      commonjs(),
      // uglify(),
      fileSize()
    ]
  })
    .then(bundle => {
      return bundle.write({
        format: 'iife', // 'amd', 'cjs', 'es6', 'iife', 'umd'
        dest: 'dist/bundle.js'
      })
    });
});

gulp.task('roll:min', () => {
  return rollup({
    entry: 'src/main.js',
    plugins: [
      babel({
        exclude: 'node_modules/**'
      }),
      nodeResolve({
        jsnext: true,
        browser: true
      }),
      commonjs(),
      uglify(),
      fileSize()
    ]
  })
    .then(bundle => {
      return bundle.write({
        format: 'iife', // 'amd', 'cjs', 'es6', 'iife', 'umd'
        dest: 'dist/bundle-min.js'
      })
    });
});

gulp.task('tsc', () => {
  return rollup({
    entry: 'src/typescript.ts',
    plugins: [
      typescript(),
      babel({
        exclude: 'node_modules/**'
      }),
      nodeResolve({
        jsnext: true,
        browser: true
      }),
      commonjs(),
      // uglify(),
      fileSize()
    ]
  })
    .then(bundle => {
      return bundle.write({
        format: 'iife', // 'amd', 'cjs', 'es6', 'iife', 'umd'
        dest: 'dist/typescript.js'
      })
    });
});
