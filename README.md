# rollup test

Test using rollup with babel + uglify

Commands

Create es6 to es5

```
# src/main.js
gulp
```

Create the uglified version of above

```
gulp roll:min
```

Typescript to es5

```
# src/typescript.ts
gulp tsc
```

Build all

```
gulp build
```
